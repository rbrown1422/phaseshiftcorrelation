# -*- coding: UTF-8 -*-
# Dr. Bob Brown         all rights reserved    to access or use contact at   rbrownf@gmu.edu

# Pearson.py -- Spearman Correlation in Python by Bob Brown for Post BINF Dissertation at GMU Aug 2010
## v0  initial implemenation
#===================================

## Galaxy changes
##      Split code to Correlation Networks and CDN for Galaxy     v34a v0 for both
##v34a v0  change scipy functions to inline code from internet and log from numpy math

#===================================
#v50 NEW Cyclic Correation functionality added 
#2019dec06  accept data converted to sine values already and ready to use directly in PSC


import os
import sys
import math        #v45
sys.path.append("~/galaxy/eggs/numpy-1.6.0-py2.7-macosx-10.6-intel-ucs2.egg/numpy")
import numpy    #v40
from scipy import ones, zeros, shape, mean, arctanh, real, log10, sqrt
import scipy.stats       #v45

#v50  TODO rename library below
#v9 import cyclicCode2routinesNoRadianV8     #v8 creates correlations between 0 and 1  no neg correlations 2016jul16 
#import CyclicCorrClass

#v45from scipy.stats import norm, chi2, t #v45 t.sf #v42 use isf or ppt KW      http://adorio-research.org/wordpress/?p=237

##from numpy.scimath import log        ## in Galaxy numpy egg
###from numpy.distutils import log      ## in Galaxy numpy egg    ##get from Galaxy
##sys.path.append("/Applications/Python\ 2.7/numpy")
##sys.path.append("/Applications/Python\ 2.7/scipy")
##import ones, zeros, shape, mean, arctanh ## real, log10, sqrt
##v0 CDN separate     from numpy import *     ## in Galaxy numpy egg        ##get from Galaxy
## from distutils import log ## in Galaxy numpy egg        ##get from Galaxy
## from scipy import *      ##v13
#v34 v0 Galaxy different eggs
##from scipy.stats.mstats import rankdata  #v9
##from scipy import *  ##v13  
## import math      ##v33 for the log function
#import csv


#======================================
#v42 redo Parse to return a class dictionary of record for every sample in the input file
#     each sample will be in a dictionary with key = row number from the original file??

def Parse_CVSfile2Dict( ssData,featureStartColNum=3, ignored=0):  #v45 calc end column
 
## Remember Python STARTS counting FROM ZERO
## create a "matrix list" of rows and fields.  A list of rows with their fields
### appended to the row list.

#### OUTPUT VALUES
#  1.classSampleDict -- dictionary of sample rows. List[i] is the data values for all features
#         for sample i+1 remember count from Zero. [i][j] is the value for feature #j+1
#         for sample i+1

#  2. sampleIDList -- a list of all the sample Names/IDs

#  3. featrNameList -- list of lists     
#         list[0] is all the feature names/ids
#         List[1][j] is a list of all the rank values for featire [j+1]in sample order NOT the rank order
#             only indicates if value is good = 0 or text/bad value =-1

#  4. error -- if 'true' a problem
    
   # split the file into lines

#v42 globals
    global colDelimiter
    global featureHdrRow #v42 now always = 1
    global SampleIdCol       #v42 now always = 1
    global minSampleReqdPercent
    global pearson
#     global sampleDataList    #v42 replaced by allSampleDict has all samples
    global sampleIDList           #v42 [rowNum from file (allSampleDict key) , sample id, then class name]
    global featrNameList
    global allDataMatrix
    global allSampleDict    #v42 allow up to n classes in one input file

    allSampleDict={}

    a = ssData.splitlines()         #####v0 CDN --->     \r       had to be used instead of \n     GO FIGURE!!!!!
    #print( 'PSC input file number of rows= '+str(len(a))  )

    featureHdrRow = 0  #v42 so samples go from rowNum 1- n
    featrNameList =[] # track valid values per column and IF ties in rank
    sampleIDList= []
    sampleDataList =[]      
    rowNum          = -1
    cohort          = ''
    cohortList      = []
    
    error         = 'false'        # verify same number of fields in each row
    firstRow    ='true'
    
    for i in a:        # for each row from spreadsheet
        tmp = i.replace('\r','') ##v22 fix /n
        b = tmp.split(colDelimiter)
        fieldList = []          

        if firstRow == 'true':      featureEndColNum = len(b)     #v45 set end col number
        #causes error rowNum +=1      #v45 count all rows even if bad       
        #print 'b[0] firstRow featureEndColNum, len(b), rowNum =',b[0],firstRow, featureEndColNum, len(b), rowNum 
        if(len(b) == featureEndColNum) and b[0] != "" and b[1] != "":       #v45 sample id and class, and all is good enough data fields
            
            if      firstRow == 'true':          #       =>featureHdrRow:        #v42 always row 1 // capture feature id names                 
                firstRow = 'false'
                for j in range(featureStartColNum-1, featureEndColNum):        #v42a featureStartColNum-1
                    fieldList.append(b[j])       
                featrNameList.append(fieldList) # r[n] n = spreadsheet row -1
                print( '~~~~~~~featrNameList len first last'+str(len(featrNameList))+" first "+ featrNameList[0][0]+" last "+ featrNameList[0][-1] )    #v42

            else: # only ALL sample rows FOR ALL COHORTS
                fieldList = []
                rowNum +=1        #v42a       start dict at zero
                fieldList.append(rowNum) 
                #v45  for j in range(0,2):       #v42 add sample and cohort (class
                #     fieldList.append(b[j])
                fieldList.append(b[0])              #v45 sample name
                fieldList.append(b[1].lower())       #v45 make all classes lower case so case doesn't matter
                sampleIDList.append(fieldList)        #v42 save sample ids and class here only 
                
                fieldList = []
                
                for j in range(featureStartColNum-1, featureEndColNum):         #v42a featureStartColNum-1
                    fieldList.append(b[j])                  #v42 !!!! 
                allSampleDict[rowNum] =fieldList    # V42 add to ONE DICTIONARY for now //add data values for sample to list

                #print "rownum,        allSampleDict[rowNum=      ",rowNum , allSampleDict[rowNum] ##v22
                        
        else:  #short row somehow but ignore empty rows
            if (rowNum >= len(a)-5) or (len(b) < 2): #v45 short = bad if hit data or header rows unless at end of file if row empty skip
            #v45if (rowNum >= featureHdrRow) and (b[0] != ''): # short = bad if hit data or header rows unless at end of file
                print( "WARNING Skipped row with sample= " +str( b[0])+ 'in row='+str(rowNum ))    #v45
            else:     
                print( "ERROR Skipped row with sample= " +str( b[0])+ 'in row='+str(rowNum ))    #v45
                error = 'true' 
                sys.exit(-1)

    #print "ssssss, len featrNameList, featrNameList 0 -1,err =", sampleIDList, len(featrNameList[0]), featrNameList[0][0],featrNameList[0][-1], error
    
    return      error


#======================================================================
def clean_Dict_Data_Values(): #v42 input global // v34 added minSampleReqdPercent

    # Given a cvs file with multiple samples, and items measured, return ranks
    # within each Item category by converting measured amounts to ranks
    # for only the contiguous samples specified.
    # scrub non-number values BUT allow if Null to = 0 for lazy data entry :)
    
    # INPUT
    # 1. sampleDataList = a matrix of rows and columns of data values from the input cvs file
    #     column                    c1              c2          c3       c4         c5
    #  Possible heading row= sample ID      metadata    Sucrose     E.Coli       fungi N
    #                          Person 1      Male        2.35        50607     985
    #      NOTE: ss can have mulitple sample types in it - disease 1,2,3, normal)
    

    # NOTE: Number of rows and columns was handled prior to this call.
    # All rows and columns in sampleDataList are for ranking
    # featrNameList = list of col header, # valid values, if rank ties or not

##v31 removed      # keepZeroValues      ##v16 fixes some errors
##v31 remove setting zero values to min real value so can remove zero pair in correlation
#v42 globals
    global colDelimiter
    global featureHdrRow
    global classCol
    global SampleIdCol
    global minSampleReqdPercent
    global pearson
#     global sampleDataList    #v42 replaced by allSampleDict
    global sampleIDList
    global featrNameList
    global allDataMatrix  
    global allSampleDict    #v42 original input data --allow up to n classes in one input file sample 1 key =1      b/c feature row =0
    global keepZeroPairs
    global normwAllVals4Feat;

##        print 'sample data list zzz=', sampleDataList[0]
#v42    nSamp = len( sampleDataList) # samples = rows
#v42    print '==== ',sampleDataList[0] , '======='

    nSamp = len( sampleIDList) # samples = rows
    nFeatr= len(featrNameList[0])# num of features being measured = columns

#v42    nFeatr= len(allSampleDict[1])# num of features being measured = columns
    print( '>>>>> number of samples= '+str(nSamp)+" number of Features="+str( nFeatr)+ '<<<<<') #v34
    
    # switch allDataMatrix to be row = all data values for 1 featr
    #      and column represents on sample source
    
    allDataMatrix= zeros((nFeatr,nSamp))    #  store values for all samples and all features with text turned to =-1
    
    validFeatrCnt= zeros(nFeatr)
    skipFeature= zeros(nFeatr)      #v34
    tmpErrList = []
    mssgList = []  #v9 added tmpErrList feeding mssgList
    #print 'DATA Cleanup messages num features & num samples, minSampleReqdPercent= ',nFeatr, nSamp,minSampleReqdPercent

    ###v34 we require 3 or more valid samples non-Zero AND must be higher that minSampleReqdPercent pair samples  30% or user selected
    floor = (nSamp * minSampleReqdPercent)-1  ##v34 convert minSampleReqdPercent to a 2 = 0,1,2 pairs
    #print 'floor =', (nSamp * minSampleReqdPercent)-1
    if floor < 2.0:
        floor = 2.0

    
    for c in range(0,nFeatr):  # num of features in list
#          print 'indix #cols', c,nFeatr
        validFeatrCnt[c] = nSamp  # track num data values are valid for Correlation
        zeroCnt= 0      #v34
        
        for s in range(0,nSamp):
            try:
                allDataMatrix[c,s] = float(allSampleDict[s][c]) #reorder in Matrix to be Feature (c) from Sample (s) start row#=1
            
                if (allDataMatrix[c,s] < 0 ): ##v31 v19 remove 'and ( keepZeroValues != 'yes'):'        ##v16 add AND clause not no => skip
                    validFeatrCnt[c]= validFeatrCnt[c]-1 #v31 #v29# decrease valid values
                    tmpErrList= []
                    allDataMatrix[c,s]= -999 #sine #v31 #v29
                    tmpErrList.append(sampleIDList[s])
                    tmpErrList.append(featrNameList[0][c])
                    tmpErrList.append(allSampleDict[s][c])
                    #tmpErrList.append(allSampleDict[s+1][c])
                    tmpErrList.append('removed data value < 0') ##v31 v29
                    mssgList.append(tmpErrList)
                else:
                    if(allDataMatrix[c,s] == 0 ): ##v34        if too many zero value skip feature correlations
                        zeroCnt= zeroCnt +1
            except ValueError: #v31 if field isn't an integer or floating point number => set to NA 
                tmpErrList= []      ##v31 if null then set to zero
                if ( allDataMatrix[c,s] == '') or ( allDataMatrix[c,s] == ' ') or ( allDataMatrix[c,s] == 'na') or ( allDataMatrix[c,s] == 'n/a'):      
                    allDataMatrix[c,s]= 0
                    zeroCnt= zeroCnt +1 #v34
                    
                    #print '..A null or single blank value set to 0 at row-col=',s,'-',c 
                    tmpErrList.append('A null or single blank value set to 0 at row-col='+str(s)+'-'+str(c)) 
                else:
                    if str(allDataMatrix[c,s]).lower() == 'na' or str(allDataMatrix[c,s]).lower()== 'n/a':
                        tmpErrList.append(sampleIDList[s])
                        tmpErrList.append(featrNameList[0][c])
                        tmpErrList.append(allSampleDict[s][c])
                        tmpErrList.append('Invalid non-number, treated like NA, and set to -1 and not used') ##v9
#2019dec08                    allDataMatrix[c,s]= -1       # replace text or junk with -1 == 'NAN' not a number
                    allDataMatrix[c,s]= -999       # replace text or junk with -1 == 'NAN' not a number
                    validFeatrCnt[c]= validFeatrCnt[c]-1 #v31 decrease valid values

                mssgList.append(tmpErrList) ## add errror to total error list
                
                ## v16 do not set to min positive value but leave zeros as zeros for spearman ranking
                ##substitute min value for COLUMN non-values or zeros if missingValueUseMin != no
    
        if(validFeatrCnt[c] - zeroCnt < floor ):   #v34 zerocnt is zeroPairCnt -add so correlation skips feature if not enough values
            skipFeature[c]= 1
    #v50 add fileInfo
            #print 'skip feature= ',featrNameList[0][c], ' zerocnt= ',zeroCnt,' not enough values above floor= ', floor
        else:
            skipFeature[c]= 0

    featrNameList.append(validFeatrCnt)
    featrNameList.append(skipFeature)     #v34 add new list to featrNameList for skipFeature array
    

    print( "DATA cleanup end ! total invalid values removed OR set to zero ="+str(len(mssgList)) )
#      print " featrNameList in rank samples= ", featrNameList
           

    return mssgList

#=================================
#======================================================================
#v42 Add Kustal Wallis analysis of values of a feature across all classes and evaluate significance 
#      impact of type 1 and type 2 errors

def KW(foutInfo): 
#
#v42 accept the rank matrix of all the samples 
#v42 globals
#v42    global sampleDataList
    global sampleIDList
    global featrNameList
    global allDataMatrix
    global allSampleDict    #v42 allow up to n classes in one input file
    global KWprobCutoff #v43  'N' no  'Y' yes perform KW

    #KWprobCutoff = 0.05
    
# classList structure
#     0 class name
#     1 start row number (key) in allSampleDict
#     2 end row number (key) in allSampleDict
#     3 N = number valid values for feature (non text values != float number)
#     4 Rank sum
#     5 Rank avg
#     6 Rank sum Squared avg
#     7 chi squared value

    classList  = []         #v42     each class have name, start row, end row
    tempClass =[sampleIDList[0][2],0,-2,-3,-4,-5,-6,-7,-8]      # init with first class
    #print '\n ENTER KW -- lenmsampleIDList first last',len(sampleIDList), sampleIDList[0], sampleIDList[-1]
    
    for i in range(len(sampleIDList)):
        if sampleIDList[i][2] != tempClass[0]:
            tempClass[2] = i-1              #v42a row start zero but not true in sampleDict starts key rowNum = 0 too
            tempClass[3] = tempClass[2] - tempClass[1] +1      #v50a
            classList.append(tempClass)
            #print      'tempClass[2] sampleIDList[i][2], num classes & classList' ,      tempClass[2], sampleIDList[i][2], len(classList), classList

            tempClass = [sampleIDList[i][2],i,-2,-3,-4,-5,-6]         
    
    tempClass[2] = i        #v50a add last class  #v42 row start zero but not true in sampleDict starts key rowNum = 1
    tempClass[3] = tempClass[2] - tempClass[1] +1      #v50a
    classList.append(tempClass)
    print ( 'number  classes ' +str(len(classList))+" & classList="+str( classList))
    
    if len(classList) < 2 or KWprobCutoff == 'N':     
        print( '---- KW not necessary there is only one class ---- or KWprobCutoff =N '+str(classList[0][0])+str(KWprobCutoff) )
        #foutInfo.write('---- KW not necessary there is only one class -------- or KWprobCutoff =N  '+classList[0][0]+' '+str(KWprobCutoff))
        return classList, foutInfo


#============= have classes now 
    
    nFeatr,nSamp = shape(allDataMatrix) # get # cols and # rows
    #print '\n nFeatr,nSamp = shape(allDataMatrix)= ', nFeatr,nSamp, allDataMatrix 
    
    #print( 'Feature \tclass 1\tclass 2\tPairwise Probability\tKW 0.05 cutoff\n')     
    foutInfo.write('Feature \tclass1 vs\tclass 2\tKW for clss pair\tKW cutoff p='+str(KWprobCutoff)+'\n'  )

    for i in range( 0,nFeatr):
        indx= -1
        temp= []
        featrOneOne= zeros(nSamp)
        KWprobResults = []
        
#=======#=======#=======#=======#=======#=======#=======  strip out non pos numeric and find  rank for the rest
# strip out invalid values rank = -1
#v42 walk through to see if allDataMatrix for sample feature value = -1 
#v42 then was text so exclude and reduce class sample count           

        allClassRankSum        = 0       #v50a only zero between features
        saveIndx         = [0]       #keep list of last indx used to store class values

        for numClass in range(len(classList)):      
            for row in range(classList[numClass][1], classList[numClass][2]+1):
                if (allDataMatrix[i,row] >= -1):               #v42 allDataMatrix starts at 0 not row1 
                    indx = indx +1
                    featrOneOne[indx]= allDataMatrix[i,row]
                    #print ' row indx f[i]', row, indx, featrOneOne[indx]
            saveIndx.append(indx+1)
            #print 'saveIndex featrOneOne for class end rows= ', saveIndx,featrOneOne, '~',featrNameList[0][i],classList[numClass][0],'\n'
                        
            if classList[numClass][3] < 5: 
                #print 'KW invalid n<5 Samples =',featrNameList[0][i],classList[numClass][0],'\n'
                foutInfo.write( str(featrNameList[0][i])+'\t'+str(classList[numClass][0])+'\t!!!\t!!!\t\tKW invalid n<5 Samples in feature\n')
                #KWprobResults.append( 'NO rank sum calc for this class in feature b/c n<5 ='+ classList[numClass]+ str(featrNameList[0][i])+'\n')

        featrRankAllSamp= rankdata(featrOneOne) ##v32 CREATE  Spearman Rank        return rank in same order
        #print 'featrRankAllSamp= ',featrRankAllSamp

        allValidSamplCnt= len(featrRankAllSamp)
        #print '    len(featrRankAllSamp)len(classList)', len(featrRankAllSamp), len(classList)
        
#=======#=======#=======#=======#=======#=======#=======  have rank for all samples all classes for a feature

        #print 'saveIndex =', saveIndx
        
        if allValidSamplCnt > 5:
            for classNum in range(len(classList)):
                #v50a classList[classNum][3]= saveIndx[classNum+1] -saveIndx[classNum]                          # 3 N     !!! deal with n = 0
                saveIndx[0] = 0        
                #print ' &&&&&&&& (sum(featrRankAllSamp[saveIndx[classNum]:saveIndx[classNum]',saveIndx[classNum],saveIndx[classNum+1],featrRankAllSamp[saveIndx[classNum]:saveIndx[classNum+1]]            # 4 rank sum  
                classList[classNum][4]= sum(featrRankAllSamp[saveIndx[classNum]:saveIndx[classNum+1]])      # 4 rank sum    
                classList[classNum][5]= classList[classNum][4]/classList[classNum][3]                 # 5 avg of rank sum
                classList[classNum][6]= classList[classNum][4]**2/classList[classNum][3]                # 6 avg of sq tot rank
                allClassRankSum               += classList[classNum][6]
                #print ' classNum classList 0-4 = ',classNum, classList[classNum]
            

        #=======#=======#=======#=======#=======#=======#======= Krustal-Wallis-example-v1.xlsm
            H1      = 12.0/ (allValidSamplCnt*(allValidSamplCnt+1))                     # L25=12/(E22*(E22+1))       
            H2      = H1 * allClassRankSum - 3* (allValidSamplCnt+1)          #     =L25 * J24-3*(J23+1) 
            df      = len(classList) -1         
            chi4MaxProb = scipy.stats.chi2.isf(KWprobCutoff, df)    # give chisq cutoff for given prob      -- http://adorio-research.org/wordpress/?p=237
            prob4AllClasses = scipy.stats.chi2.pdf(H2,df)                 # returns probability
            #print 'All CLASSES H1, h2=chiFeaClasses, df, chi4MaxProb, prob4AllClasses ',H1, H2,df, chi4MaxProb, prob4AllClasses
        
            if ( prob4AllClasses > KWprobCutoff): 
                #print featrNameList[0][i], prob4AllClasses , KWprobCutoff, 'KW probability is too high - no differentiation - for entire Feature'
                foutInfo.write( featrNameList[0][i]+'\tOverall too high\tOverall too high\t'+str(prob4AllClasses)+'\t'+str( KWprobCutoff)+ '\tKW probability is too high - NO differentiation - for entire Feature\n')
            else:     # continue to see if class distribution pairs are significantly diff for CDPN process later.
                foutInfo.write( featrNameList[0][i]+'\tSee Below\tSee Below\t'+str(prob4AllClasses)+'\t'+str( KWprobCutoff)+ '\tKW Probability Significant for all classes for entire Feature -- now match class pairs\n')
                #print ' KW continue'
                df = df

                #=======#=======#=======#=======#=======#=======#======= Krustal-Wallis-example-v1.xlsm
                #=SQRT((1/12)*$M$13*$J$23*($J$23+1)*((1/G23)+(1/H23))) 
                #-sqrt((1/12)* chisqvalue * ( NumtotRanks * NumtotRanks+1)) * (1/ grp 1 cnt        + 1/ grp 2 cnt
                # Determine if KW shows signficance across each pair of classes IF num classes > 2      we already did numclasses =2
        
                if      len(classList) < 3:
                    tempList= []
                    tempList.append(featrNameList[0][i])
                    tempList.append( 'across all' +str(len(classList))+' classes')
                    tempList.append('')
                    tempList.append( str(prob4AllClasses))
                    tempList.append( str(KWprobCutoff))
                    if KWprobCutoff > prob4AllClasses:      
                        tempList.append( 'only 2 Classes for Feature and are Significantly different ')
                    else:
                        tempList.append( '!! only 2 Classes for Feature and are NOT different')
            
                    KWprobResults.append(tempList)
                    #for q in range(len(classList[0])): print '**** class 0=', classList[0][q], '\n'
                    #for q in range(len(classList[1])): print '**** class 1=', classList[1][q], '\n'
            
                else:
                    temp1 = (allValidSamplCnt* (allValidSamplCnt+1))/12.0        #not divide by 2 !!! see comments on webpage Tom M march 14 2014
            
                    for c in range(len(classList)): 
                        for d in range(c+1,len(classList)):
                            try:
                                if classList[c][3] >5 and classList[d][3] >5:      
                                    tempList= []
                                    chi4ClassPair     = abs(classList[c][5]- classList[d][5])      # class chi value =diff of rankSumaverages
                                    twoClassProb    = scipy.stats.chi2.pdf(chi4ClassPair,1)                   #v46       # returns probability
                        
                                    temp2                 = (1.0/classList[c][3])+(1.0/classList[d][3])

                                    pairChiCutoff = sqrt ( chi4MaxProb* temp1 * temp2 )
                                    pairProbCutoff      = scipy.stats.chi2.pdf(pairChiCutoff,2)                     #v46 put stats. # returns probability
                                    #print 'temp1, chi4MaxProb temp2',temp1, chi4MaxProb, temp2

                                    tempList.append(featrNameList[0][i])
                                    tempList.append( classList[c][0])
                                    tempList.append( classList[d][0])
                                    tempList.append( str(twoClassProb))
                                    tempList.append( str(pairProbCutoff))
                                    #tempList.append( str('%.2f'%twoClassProb))
                                    #tempList.append( str('%.2f'%pairProbCutoff))
                                    #print 'chi4ClassPair, twoClassProb, pairChicutoff, pairProbCutoff','%.2f'%chi4ClassPair, '%.2f'%twoClassProb, '~',        '%.2f'%pairChiCutoff, '%.2f'%pairProbCutoff
                                    #if chi4ClassPair > pairChiCutoff:      
                                    if pairProbCutoff > twoClassProb:  
                                        #tempList.append( ' this class pair for feature IS Significantly different Classes')
                                        tempList= [] # do not print
                                    else:
                                        tempList.append( '!! this class pair for feature NOT different')
                                    if tempList != []:  KWprobResults.append(tempList)
                            except:
                                junk = 0   # skip likely because a class doesn't have enough values

        #=======#=======#=======#=======#=======#=======#======= write results for feature -- Krustal-Wallis-example-v1.xlsm                            
        for n in range(len(KWprobResults)): 
            for q in range(len(KWprobResults[n])): foutInfo.write(      str(KWprobResults[n][q])+'\t')
            foutInfo.write( '\n')
            
        #for n in range(len(KWprobResults)): foutInfo.write(    '\t'.join(str(KWprobResults[n])))
            
        #for n in range(len(KWprobResults)):     print KWprobResults[n] ,' ~ '      #,'\t'
        #print',,,,,,,,,,', classList
    foutInfo.write(' --------- End of KW analysis ------ \n\n') 
    
    return      classList, foutInfo

#======================================================================
#=====================================================================
#======================================================================
# 7oct2013 rank routines from stackoverflow.com/questions/3071415/efficient-method-to-calculate-the-rank-vector-of-a-list-in-python
def rank_simple(vector):
    
    return sorted(range(len(vector)), key=vector.__getitem__)

def rankdata(a):
    n = len(a)
    ivec=rank_simple(a)
    svec=[a[rank] for rank in ivec]
    sumranks = 0
    dupcount = 0
    newarray = [0]*n
    for i in range(n):
        sumranks += i
        dupcount += 1
        if i==n-1 or svec[i] != svec[i+1]:
            averank = sumranks / float(dupcount) + 1
            for j in range(i-dupcount+1,i+1):
                newarray[ivec[j]] = averank
            sumranks = 0
            dupcount = 0
    return newarray

#======================================================================
# the 7 oct 2013 norm routine from http://stackoverflow.com/questions/8669235/alternative-for-scipy-stats-norm-pdf
def normpdf(x, mu=0, sigma=1):
    u = float((x-mu) / abs(sigma))
    y = exp(-u*u/2) / (sqrt(2*pi) * abs(sigma))
    return y

#======================================================================
def pCalc( r, n):  #v45 new
# find student t then the probability
# r is correlation      n is number samples
    if n < 3: 
        pval = 1.0
    elif abs(r) > 0.999:
        pval = 0.0
    else:
        top = 1-r*r         # r= rho
        bot = n-2
#        temp = abs(r)/math.sqrt(top/bot)
        temp = abs(r)/sqrt(top/bot)
        pval = scipy.stats.t.sf(numpy.abs(temp),9)*2   # single side
    
    #print " prob calc =", r, n, pval
    
    return pval

#======================================================================
def PSC_Calc_and_StndErr( cls, classList, mssgList ):        ##v32 v19 added minSampleReqdPercent

# NOTE ##v31  uses raw data for Pearson, or Rank Data supplied to routine, for Spearman Rank correlation

# 1.    take allDataMatrix
# 2.    featrNameList list Feature Name[0], #tot feature valid values [1]  skipFeature flag [2]
##v31
# 3.   keepZeroPairs ='N' ##v31 added IF both correlation pairs are zeros exclude pair from correlation
# output
# p value no good if dataset < 500 samples
##v29 and up Pearson correlation used that keeps values NOT ranks in routine output
    # allDataMatrix structure -- each column is rank order of sample values if =-1 drop for NO to keepZeroPairs
    # sampleID, rank, ExperimentQuantity, itemID=(featr, bac, funqi, other)
##v32 added pearson param
    # input 4. pearson = 'yes' then pearson else Spearman

#v42 globals
    global colDelimiter
    global featureHdrRow    # always first row
    global classCol           # always second col
    global SampleIdCol      # always first col
    global minSampleReqdPercent
    global pearson
    global sampleDataList
    global sampleIDList
    global featrNameList
    global allDataMatrix
    global allSampleDict    #v42 allow up to n classes in one input file
    global keepZeroPairs
    global minAbsCorrVal    #v10 added min either pearson or Cyclic corr to add to output file
    global featSample4ChartFH     #uc mucosa chfile of feature1 feature2 pairs that meet min sample no outliers    psc corr>0.8 peasron < 0.8 to use to create charts
    global normwAllVals4Feat   #2019dec06 

    if normwAllVals4Feat: print(" \n>>>> CREATE NORMALIZE VALUES FOR EACH FEATURE (NOT USING ZEROS IN MINIMUM CALCUATION) \n  UP FRONT >>> BEFORE <<< PAIRING NON-ZERO Feature A and Feature PAIRS <<<\n\n")
    print("PARAMETER SETTINGS>>> minSampleReqdPercent maxOutliers minAbsCorrVal keepZeroPairs= "+str(minSampleReqdPercent)+"  "+str( maxOutliers)+"  "+str( minAbsCorrVal)+"  "+keepZeroPairs+"\n")
    
# classList structure
#     0 class name
#     1 start row number (key) in allSampleDict
#     2 end row number (key) in allSampleDict
#v43    3 N = number sample rows for a class
#NO 3 N = number valid values for feature (non text values != float number)
#     4 Rank sum
#     5 Rank avg
#     6 avg Rank sum Squared

    #v42 nFeatr,nSamp = shape(allDataMatrix) # get # cols and # rows
    nFeatr    = len(featrNameList[0])
    nSamp     = classList[cls][3]
    #print( 'Line580 nSamp ,len feature names, and feature names= '+str(nSamp)+" "+str(len(featrNameList[0])) )   #,featrNameList[0]
    
    corrList = []
    corrResultRowCnt= 0
    stndErr= -1
    cnt=0
    badCnt= 0
    ###v19 we require 3 or more valid samples non-Zero AND must be higher that minSampleReqdPercent pair samples  30% or user selected
    floor = (nSamp * minSampleReqdPercent)-1  ##v19 convert minSampleReqdPercent to a 2 = 0,1,2 pairs
    if floor < 2.0:
        floor = 2.0
    #print 'in PSC_Calc num features /num samples / min reqd sample pairs=',nFeatr,nSamp, floor+1 ##v31 v19
    #print 'Spearman for loop classList[cls][1], classList[cls][2]+1=',classList[cls][1], classList[cls][2]+1

    rho = 0 ##v19 initialize sooner
    
    for i in range( 0,nFeatr-1):   #v12 cyclic corr
    #for i in range( 0,nFeatr-2):    #v43
        #print 'PSC_Calc first feature of pair loop through others= ',featrNameList[0][i]
        #if (i/20)*20 == i: print 'PSC_Calc first feature of pair loop through others= ',featrNameList[0][i]
        
        for j in range(i+1,nFeatr):           #v12 cyclic corr
        #for j in range(i+1,nFeatr-1):                          
            indx= -1
            #v43 eror print ' featrNameList 1&2', i,featrNameList[0][1],j, featrNameList[0][2]
            #print( '\n\nNEW pair Line608 i,j, len featrNameList[0][j] '+str(i)+" "+str(j)+"\n"+str(featrNameList))           #v43 , featrNameList[1]

            #if (featrNameList[2][i] == 0 and featrNameList[2][j] == 0):   #v43 says both values are legal numeric
            if (featrNameList[1][i] > -1 and featrNameList[1][j] > -1):         #v43 says both values are legal numeric
                temp= []
                featrOneOne= zeros(nSamp)
                featrTwoOne= zeros(nSamp)

                # strip out invalid values rank = -1
                for k in range(classList[cls][1], classList[cls][2]+1):          #v43 uncomment need actual rows not alway 0 t0 nSamp
                
#v43 WRONG need to go to specific row in datamatrix                   for k in range(0,nSamp):       

                    try:

#v10  if keepZeroPairs = Y => one OR the other, but not both, of the feature pair CAN be zero
#v10  if keepZeroPairs = N => BOTH feature pair value must be NON-zero
                        zeroVal= 'false'
                    ##sine 2019dec03
                        if (allDataMatrix[i,k] >= 0) and (allDataMatrix[j,k] >= 0): #     because one of pair is invalid if < 0
                            if normwAllVals4Feat:
                                indx = indx +1
                                featrOneOne[indx]= allDataMatrix[i,k]
                                featrTwoOne[indx]= allDataMatrix[j,k]
                                #if indx == 1: print("line635  index featrOneOne featrTwoOne="+str(indx)+"  "+str(featrOneOne)+" "+str(featrTwoOne))
                           # elif ((allDataMatrix[i,k]<= 0 ) and (allDataMatrix[j,k]<= 0 )):  #  and (keepZeroPairs == 'Y'):
                           #     zeroVal = 'true'      
                            elif ((allDataMatrix[i,k]<=0 ) or (allDataMatrix[j,k]<= 0 )) and (keepZeroPairs == 'N'):      
                                zeroVal = 'true'      
                            else:
                                indx = indx +1
                                featrOneOne[indx]= allDataMatrix[i,k]
                                featrTwoOne[indx]= allDataMatrix[j,k]
                    except:
                        print("!!!! failed in rows 600-640, nSamp,indx i j, k allDataMatrix ="+str(nSamp)+" "+str(indx)+" "+str(i)+" "+str(j)+" "+str(k)+" "+str( allDataMatrix[i,k])+" "+str(featrOneOne)+" "+str(allDataMatrix[j,k])+" "+str(featrTwoOne))
                        return 
                                
                #r,p= scipy.stats.spearmanr(featrOne, featrTwo) ## assumes no ties???
                #print ' spearman class and nsamp counted=', classList[cls][0], indx

               # print("line 651 index="+str(indx))
                if indx > floor:   

                    corrResultRowCnt= corrResultRowCnt +1
                    temp.append(corrResultRowCnt)
                    temp.append(classList[cls][0])       #v42a inc class name for CDPN ease
#=======#=======#=======#=======#=======#=======#======= 
#=======#=======#=======#=======#=======#=======#======= 
#v 50 add cyclic correlation modules
                    
                    outlier, normfeatrOne, normfeatrTwo, pearsonsRho, meanFeatrOne, meanFeatrTwo, featrOneNonZeroList,featrTwoNonZeroList= Normalize_Values(featrOneOne, featrTwoOne)
                    #print("post Normalize floor len(normfeatrOne) len(normfeatrTwo)floor outlier "+str(floor)+" "+str(len(normfeatrOne))+" "+str(len(normfeatrTwo))+" "+str(outlier))
#ucmucosa                     if len(normfeatrOne) > 0 and len(normfeatrTwo) > 0:        #v10 add check for null lists
                    if( len(normfeatrOne) > floor and len(normfeatrTwo) > floor and not outlier):         #are 38 UC samples total
                        keepPredPoint, keepDiffPredPt,keepCalcDiffPt,keepCumDiffPt,rho, leadFeature, lagPhase, status= PSC_Correlation(normfeatrOne, normfeatrTwo)
                    else:
                        status= 'outlier_or_below_pairs_floor'     #v10
                        #print("OUTLIER or not enough Samples found for lead lag Features="+featrNameList[0][i]+featrNameList[0][j])

#ucmucosa only want psc where linear is below abscorr to accent differences
#ucmucosa                     if status == 'good' and rho != -99 and (abs(rho) > minAbsCorrVal and abs(pearsonsRho) < minAbsCorrVal) :       # -99 is no correlation
                    if status == 'good' and rho != -99 and (abs(rho) > minAbsCorrVal or abs(pearsonsRho) > minAbsCorrVal) :       # -99 is no correlation
#v10 add cutoff           if status == 'good' and rho != -99:         # -99 is no correlation
#v50 print lead feature first 1 is first 2= second
                        if leadFeature == 1:
                            temp.append(featrNameList[0][i]) # 1st featr name
                            temp.append(featrNameList[0][j]) # 2nd featr name
                            lenFeatr= len(normfeatrOne) #v26
                            temp.append(lenFeatr)  ##v13     add number of pairs since featr A & B must be same as pairs
                            temp.append(str('%.3f'%rho)) ##v50 circular corr value.
                            temp.append(lagPhase)     #v7 NEW phase shift between feat 1 and 2
                            temp.append(str('%.3f'%pearsonsRho)) ##v50 circular corr value.
    
    #v51 change print if mean is very small
#                             if meanFeatrOne <= 1.0:
#                                 temp.append('%.3f'%meanFeatrOne)                #v42 non rank mean added
#                             else:
#                                 temp.append('%.1f'%meanFeatrOne)            
#                             if meanFeatrTwo <= 1.0:
#                                 temp.append('%.3f'%meanFeatrTwo)                #v42 non rank mean added
#                             else:
#                                 temp.append('%.1f'%meanFeatrTwo)                #v42 added
                        else:     #v12 add switch lead feature
                            temp.append(featrNameList[0][j]) # 2nd featr name
                            temp.append(featrNameList[0][i]) # 1st featr name
                            lenFeatr= len(normfeatrOne) #v26
                            temp.append(lenFeatr)  ##v13     add number of pairs since featr A & B must be same as pairs

                            temp.append(str('%.3f'%rho)) ##v50 circular corr value.
                            temp.append(lagPhase)     #v7 NEW phase shift between feat 1 and 2
                            temp.append(str('%.3f'%pearsonsRho)) ##v50 circular corr value.
    
    #v51 change print if mean is very small
#                             if meanFeatrTwo <= 1.0:
#                                 temp.append('%.3f'%meanFeatrTwo)                #v42 non rank mean added
#                             else:
#                                 temp.append('%.1f'%meanFeatrTwo)                #v42 added
#                             if meanFeatrOne <= 1.0:
#                                 temp.append('%.3f'%meanFeatrOne)                #v42 non rank mean added
#                             else:
#                                 temp.append('%.1f'%meanFeatrOne)            

                        temp.append(str('%.3f'%pCalc( rho,lenFeatr)))       ##v45 probability for correlation result.                        
# if below true add data to file for creating charts
                        if abs(pearsonsRho) < 0.4 and abs(rho) > 0.80:
        #                    temp.append('Z')                #v10 classify rhos added
        #                elif abs(rho) > minAbsCorrVal:
         #               if True:
#mucosa print data for chart
                           # chartData(temp,featrOne,featrTwo)
                            cnt=0
 #                           featSample4ChartFH.write(str(temp[0])+"\t0\t Lead Sample \t Lag Sample\t Predicted Lag Value\t \tdiff lag & pred Lag \tCUM lag diff \n")
                            #featSample4ChartFH.write("\n"+str(temp[0])+"\t0\t Cohort= "+temp[1]+"\n")
                 
                           # featSample4ChartFH.write(str(temp[0])+"\t"+str(cnt)+"\t")
# add sample id and raw values later to chart file                         keepPredPoint, keepDiffPredPt,keepCalcDiffPt,keepCumDiffPt
                            if( leadFeature ==1): 
                                featSample4ChartFH.write(str(temp[0])+"\t"+str(cnt)+"\t"+str(temp[2])+"\t"+str(temp[3])+"\t"+str(temp[2])+"\t"+str(temp[3])+"\tpredicted_"+str(temp[3])+"\t"+temp[1]+"\t")
                            else: 
                                featSample4ChartFH.write(str(temp[0])+"\t"+str(cnt)+"\t"+str(temp[3])+"\t"+str(temp[2])+"\t"+str(temp[3])+"\t"+str(temp[2])+"\tpredicted_"+str(temp[2])+"\t"+temp[1]+"\t")
                            
                            for t in range(3):
                               featSample4ChartFH.write(str(temp[t+5])+"\t")
                            featSample4ChartFH.write("\n")
                            
#wtf                            if keepPredPoint != []:
                            for zz in range(len(normfeatrOne)):
                                cnt +=1
                                if( leadFeature ==1): 
                                    featSample4ChartFH.write(str(temp[0])+"\t"+str(cnt)+"\t"+str(featrOneNonZeroList[zz])+"\t"+str(featrTwoNonZeroList[zz])+"\t"+str(normfeatrOne[zz])+"\t"+str(normfeatrTwo[zz])+"\t"+str(keepPredPoint[zz])+"\n")
                                    #print("line737 1 zz "+str(zz)+" keepPredPoint="+str(keepPredPoint)+"\n")
                                    #featSample4ChartFH.write("\t"+str(keepPredPoint[zz])+"\n")
                                    #featSample4ChartFH.write("\t"+str(keepPredPoint[zz])+"\t \t"+str(keepDiffPredPt[zz])+"\t"+str(keepCumDiffPt[zz])+"\n")
                                else:
                                    featSample4ChartFH.write(str(temp[0])+"\t"+str(cnt)+"\t"+str(featrTwoNonZeroList[zz])+"\t"+str(featrOneNonZeroList[zz])+"\t"+str(normfeatrTwo[zz])+"\t"+str(normfeatrOne[zz])+"\t"+str(keepPredPoint[zz])+"\n")
                                    #print("line742 2 zz "+str(zz)+" keepPredPoint="+str(keepPredPoint)+"\n")
                                    #featSample4ChartFH.write("\t"+str(keepPredPoint[zz])+"\n")
                                    #featSample4ChartFH.write("\t \t"+str(keepDiffPredPt[zz])+"\t"+str(keepCumDiffPt[zz])+"\n")

                            #for i in range(nSamp-len(normfeatrOne)):
                             #   cnt +=1
                            #featSample4ChartFH.write(str(temp[0])+"\t"+str(cnt)+"\n") # so all chart rows are equal length to copy chart format
                            #temp.append('')                #ONLY PSC > corr min NOT pearson
 #mucosa                           temp.append('C')                #ONLY PSC > corr min NOT pearson
#                        else: 
#                            temp.append('L')                #v10 classify rhos added

                        corrList.append(temp)  ##v10 move here -- add corr results to others
                        #print 'Temp values after corr calc=', temp

#v50 add error below
                    if status != 'good' and status != "outlier_or_below_pairs_floor":
                        badCnt += 1   # print total later
                        print ('Line766 BAD status Features= '+status+" "+ str(featrNameList[0][i])+" "+ str(featrNameList[0][j])+"\n" )
                       
            else:
                tmpErrList= []
                tmpErrList.append(featrNameList[0][i]) # 1st featr name        #v9
                tmpErrList.append(featrNameList[0][j]) # 2nd featr name
                tmpErrList.append('allowed featr pairs of '+str(indx+1)+' < '+str(floor)+' minimum samples,user defined, minsample% =' \
                            + str(minSampleReqdPercent))
                mssgList.append(tmpErrList) ##v19


            #v43 for s in range(0,len(corrList)): print corrList[s]

#      print " featrNameList& pearson in PSC_Calc_and_StndErr= ", featrNameList, pearson
    if badCnt > 0:
        tmpErrList= []
        tmpErrList.append(featrNameList[0][i]) # 1st featr name        #v9
        tmpErrList.append(featrNameList[0][j]) # 2nd featr name
        tmpErrList.append("ERROR "+str(badCnt)+' total errors in circular correlation calcuation')
        mssgList.append(tmpErrList) #v50

    return corrList, mssgList     ##v31 added featrNameList

#======================================================================
#class PSC_CorrelationClass:
#        def __init__(self):
#                self.name = 'PSC_Correlation'


#======================================================================
def Normalize_Values(featrOneList, featrTwoList):
    # We need to normalize the data values to a range of 0 to pi 
    # then convert these values to angle radians to compare later.
    # These values will allow for output values from 0 to pi radians
    # SINE info we can do two ways only use non-zero Feature A&B pairs to define normalize values OR
    # normwAllVals4Feat=true use Feature A non-zero to find its normailzed values then do same featiure B then
    #     return only value where both A&B were both non-zero values in the pairs
    global maxOutliers      #uc mucosa change be able to set (currently hardcode) max outlier     only normalized scores > 0.1 or only < 0.1 pairs to output for Chart AND psc output
    global normwAllVals4Feat   #2019dec06     
    
    #print ' start Normalize  list 1 and 2 \n', featrOneList,"\n", featrTwoList
    outlier             = False      #uc mucosa change
    cntSampGT0pt1       = 0
    cntSampLTneg0pt1    = 0
    cntSampGT0pt2       = 0
    cntSampLTneg0pt2    = 0
    featrOneNormList    = []
    featrTwoNormList    = []
    featrOneNonZeroList = []
    featrTwoNonZeroList = []
    featrOneMean        = 0
    featrTwoMean        = 0
    rho                 = 0
    badCnt              = 0
    maxF1= 1.0*max(featrOneList)     # these may not be true min and max values but are snapshots in time from sample on hand
#30apr19    minF1 = 1.0*min(featrOneList) 
    maxF2= 1.0*max(featrTwoList)  # these may not be true min and max values but are snapshots in time from sample on hand
#30apr19     minF2= 1.0*min(featrTwoList) 
    minF1= 999999.0
    minF2= 999999.0
    #print ' f1 min max        f2 min max= ', minF1, maxF1, minF2, maxF2
#dec2019  find min use entire feature A non-zero min OR only non zeor min if Feat B is not zero
    for i in range(len(featrOneList)):
        if featrOneList[i] != 0:
            if featrOneList[i] < minF1:
                minF1= featrOneList[i]
        if featrTwoList[i] != 0:
            if featrTwoList[i] < minF2:
                minF2= featrTwoList[i]
   
    if maxF1 >= minF1 and maxF2 >= minF2 :
        halfRange1 = (maxF1- minF1)/2.0
#        norm1  = maxF1- halfRange1        
        halfRange2 = (maxF2- minF2)/2.0
#        norm2 = maxF2- halfRange2
        #print("mid vals="+str(halfRange1)+" "+str(halfRange2))

        for i in range(0, len(featrOneList)):
#2019dec06  at this point only normwAllVals4Feat has zero values so remove them (0s) and their pair
            if featrOneList[i] != 0 and featrTwoList[i]!= 0:  #keep only non-zero pairs
#                temp1  = float("%0.5f" %((1.0*featrOneList[i]- 1.0*halfRange1-1.0*minF1) / (1.0*halfRange1)))
                temp1  = (featrOneList[i]- halfRange1 -minF1)/(halfRange1)
                if badCnt == 1:
                    badCnt +=1
                    print("line851 errorfeatrOneList[i]- halfRange1 -minF1 / halfRange1= "+str(featrOneList[i])+" "+str( halfRange1)+" "+str(minF1)+" "+str(halfRange1) )
                if(temp1 > 0.1 ): cntSampGT0pt1    +=1
                if(temp1 < -0.1): cntSampLTneg0pt1 +=1
                #print("line 850="+str(temp1))
                if temp1 > 1.0000000: temp1= 0.999999
                elif temp1 < -1.0000000: temp1= -0.999999
                
                featrOneNormList.append( temp1 )
                #print 'F1      featrOneNormList', featrOneNormList
 #               temp2  = float("%0.5f" % ((1.0*featrTwoList[i]- 1.0*halfRange2- 1.0*minF2)/(1.0*halfRange2)))
                temp2  = (1.0*featrTwoList[i]- 1.0*halfRange2- 1.0*minF2)/(1.0*halfRange2)
                if(temp2 > 0.1 ): cntSampGT0pt2    +=1
                if(temp2 < -0.1): cntSampLTneg0pt2 +=1
                if temp2 > 1.0000000: temp2= 0.999999
                elif temp2 < -1.0000000: temp2= -0.999999

                featrTwoNormList.append( temp2 )
            #print 'F2      featrTwoNormList', featrTwoNormList
#jan2020 realize messed up mean when switch to norm values  save original non zero values
                featrOneNonZeroList.append(featrOneList[i])
                featrTwoNonZeroList.append(featrTwoList[i])
            
        if(cntSampGT0pt1 <= maxOutliers or cntSampLTneg0pt1 <= maxOutliers): 
            outlier= True
#            print("outlier= True featrOneNormList=") # +str(featrOneNormList))
        if(cntSampGT0pt2 <= maxOutliers or cntSampLTneg0pt2 <= maxOutliers): 
            outlier= True
#            print("outlier= True featrTwoNormList=") #+str(featrTwoNormList)+ str(featrTwoList))

#jan2020 realize messed up mean when switch to norm values  save original non zero values
#calcuate Pearson Correlation for non zero pairs    
        if len(featrOneNormList) > 0 and len(featrTwoNormList) > 0:
            featrOneMean= sum(featrOneNonZeroList)/len(featrOneNonZeroList)
            featrTwoMean= sum(featrTwoNonZeroList)/len(featrTwoNonZeroList)
            top    = 0
            bottom1 = 0
            bottom2 = 0
    
            for i in range(len(featrOneNonZeroList)):
                f1diff= featrOneNonZeroList[i]-featrOneMean
                f2diff= featrTwoNonZeroList[i]-featrTwoMean
                top     += f1diff * f2diff
                bottom1 += f1diff**2
                bottom2 += f2diff**2
    
            if bottom1 <= 0.0000000001 or bottom2 <= 0.0000000001:      ##v31 extra zero# v20     real number not exactly zero
                rho = -99
            else:
                rho= top/(sqrt(bottom1)*sqrt(bottom2))     
        else:
            rho = -99
# v50 NP Spearman for SINE PSC old linear correlation function
#                     if True == False and pearson != 'yes':      #v12 do not use for Cyclic Corr ##v31 if TRUE then Spearman Rank applied
#                         featrTemp= featrOne
#                         featrOne= rankdata(featrTemp) ##v32 CREATE      Spearman Rank
#                         featrTemp= featrTwo
#                         featrTwo= rankdata(featrTemp) ##v32 CREATE Spearman Rank
                            
    #else: print ( 'min max are == ', maxF1, minF1, ' and' , maxF2, minF2)
    #print( "\nend Normalize list 1 and 2 "+ str(featrOneNormList)+"\n"+ str(featrTwoNormList)+"\n\n")

    return outlier, featrOneNormList, featrTwoNormList, rho,featrOneMean,featrTwoMean,featrOneNonZeroList,featrTwoNonZeroList    # return Y values for feature 1 & 2 along curve

#======================================================================
def PSC_Correlation(featr1ValueList, featr2ValueList):
    # take the two arrays and determine if they are correlated by a sin function with a delay (phase shift)
    # Likely one feature will lead the other if there is causality and if the phase shift is 
    # significant a standard Pearson correlation may not give a high value.        
    # When you have A -> B -> C        it is likely Pearson or Sperman will correlate A to B and B to C but
    # not A to C as the phase shift is too large.
    # We will try with feature 1 leading by a phase shift of 0 up to pi/2 in 1/8th increments then
    # repeat with feature 2 leading
    # The "correlation" is a little trickier since the sin will change quickly in the middle of the wave
    # vs more slowly at the peak and valley so I will add a fudge factor to compensate.
    #v7 calculate correlation to be in range 0 t0 1
    
    # NOTE: needs python math library
    #math.sin(math.pi/2)

    pie = math.pi
    phaseShift = [0, pie/8, pie/4, 3*pie/8 , pie/2, 5*pie/8, 3*pie/4, 7*pie/8, pie]
#    phaseShift = [0]
    phaseShiftPrint= ['0','22.5', '45', '67.5', '90' ,'112.5', '135', '157.5', '180' ] 

#v11    phaseShift= [0,pie]
#v11    phaseShiftPrint = ['Zero','180'] 
#     phaseShiftPrint = ['Zero Pi','Pi/8', 'Pi/4', '3/8ths Pi', 'Pi/2' , '5/8ths Pi', '3/4ths Pi', '7/8ths Pi', 
#                 'inverse Pi' , 'inverse 9/8ths Pi', 'inverse 5/8ths Pi', 'inverse 11/8ths Pi', 'inverse 3/2ths Pi' , 
#                 'inverse 13/8ths Pi', 'inverse 7/4ths Pi', 'inverse 15/8ths Pi']
    
    leadFeature = 99
    nPSC_Correlation = -9999
    status          = 'good'
    saveCnt           = -99
    lagPhaseSave = 'empty'
    #print ' start cyclic corr list 1 and 2 \n', featr1ValueList,"\n", featr2ValueList
    try:    
        keepPredF1Point=[]
        keepDiffPredPt1=[]
        cnt = -1
        
        for phz in phaseShift:
            #phz2to1           = -phz  do with f1or2 later
            sumDiffPredPt1  = 0.0
            sumDiffPredPt2  = 0.0
#            keepPredPoint= []
            
            tempPredF1Point=[]
            tempDiffPredPt1=[]
            tempPredF2Point=[]
            tempDiffPredPt2=[]
            tempCalcDiffPt1=[]
            tempCumDiffPt1 =[]
            tempCalcDiffPt2=[]
            tempCumDiffPt2 =[]
            cnt             +=1
            #print 'start f1or2 phzShift       ',f1or2, "%0.2f" % phz
            numPts =len(featr1ValueList)
            
            for k in range(0, numPts):                    
                diffPredPt1     = 0.0
                diffPredPt2     = 0.0
                predF1Point     = 0.0
                predF2Point     = 0.0
                keep     = k
                diffPredPt1= 0
                diffPredPt2= 0
                #if i == 1:       # see if feature 1 leads 
                for f1or2 in [-1.0,1.0]:     #19aapr30 ,-1.0]: #v12 combined two runs in one #first feature lead     then have second feature lead
         #print '\n lead lag ',f1or2
                    #try:
                    #print("line977 error in PSC_Correlation math.cos f1or2 phz featr1ValueList[k] featr2ValueList[k]= "+str(f1or2)+" "+str(phz)+" f1="+str(featr1ValueList[k])+" "+str(featr2ValueList[k])+"\n")
                    priorPred2Pt= predF2Point
    #                       print("here line976+featr1ValueList[k])+ f1or2*phz="+str(featr1ValueList[k])+" "+str(f1or2)+" "+str(phz)) 
                    predF2Point = math.cos(math.acos(featr1ValueList[k])+ f1or2*phz)
                    #predF2Point = float("%0.5f" % (math.cos(math.acos(featr1ValueList[k])+ f1or2*phz)))
                    #print("here line976+predF2Point"+str(predF2Point))    #2019dec06 normwAllVals4Feat
                    if abs(predF2Point) > 1.0:     print(' error in predF2Point > 1='+str(predF2Point)+'\n')
                        #2019dec06 normwAllVals4Feat
                    if predF2Point > 1.0 : predF2Point     = 1.0
                    elif predF2Point < -1.0 : predF2Point = -1.0
                    
    
                    #if k==0: print 'line986 featr1, (actual) featr2, (predicted) predF2Point = ',phaseShiftPrint[cnt],"%0.2f" % featr1ValueList[k], '(actual)', "%0.2f" % featr2ValueList[k], '(predicted)',"%0.2f" % predF2Point
    #                       print("here line987+")    #2019dec06 normwAllVals4Feat
                    priorPred1Pt= predF1Point
    #2019dec06 normwAllVals4Feat
                    #print("line993 error in PSC_Correlation math.cos f1or2 phz featr1ValueList[k] featr2ValueList[k]= "+str(f1or2)+" "+str(phz)+" f1="+str(featr1ValueList[k])+" f2="+str(featr2ValueList[k])+"\n")
                    predF1Point = math.cos(math.acos(featr2ValueList[k])+f1or2*phz )
                        #print("here line991+predF1111Point"+str(predF1Point))    #2019dec06 normwAllVals4Feat
    #                    except Exception as e:
    #                         print("line 993 error in PSC_Correlation math.cos f1or2 phz featr1ValueList[k] featr2ValueList[k]= "+str(f1or2)+" "+str(phz)+" "+str(featr1ValueList[k])+" "+str(featr2ValueList[k])+"\n")
    #                         sys.stdout("line 994error in PSC_Correlation math.cos = "+ repr(e))        #v10 keep ,
    #                         status = 'bad_PSC_calc_error'
    #                         #return 0, 0, pie, status
    #                         return keepPredPoint, keepDiffPredPt,keepCalcDiffPt,keepCumDiffPt, 0, saveleadFeature, lagPhaseSave, status
                                        
                    if abs(predF1Point) > 1.0:     print( ' error in predF1Point > 1='+str(predF1Point)+'\n')
                    if predF1Point > 1.0 : predF1Point    = 1.0
                    elif predF1Point < -1.0 : predF1Point = -1.0       
                                
    #-#-# do  for second point laging  see if st point rising 2nd pt lower value or 1st pt sinking 2nd point greater value
                diffPriorPredPt2 = abs((featr2ValueList[k]) - priorPred2Pt)      # predicted value - actual value
                diffPredPt2 = abs((featr2ValueList[k]) - predF2Point)    # predicted value - actual value
                if(diffPriorPredPt2 < diffPredPt2):
                    calcDiffPt2= diffPriorPredPt2
    #                   calcDiffPt2= diffPriorPredPt2**2
    #                   calcDiffPt2 = diffPriorPredPt2/(4-3*abs(priorPred2Pt))
                    sumDiffPredPt2 += calcDiffPt2
                    tempPredF2Point.append(priorPred2Pt)
                    tempDiffPredPt2.append(diffPriorPredPt2)
                else:
                    calcDiffPt2 = diffPredPt2
    #                   calcDiffPt2 = diffPredPt2**2
    #                   calcDiffPt2 = diffPredPt2/(4-3*abs(predF2Point))
                    sumDiffPredPt2 += calcDiffPt2
                    tempPredF2Point.append(predF2Point)
                    tempDiffPredPt2.append(diffPredPt2)
                tempCalcDiffPt2.append(0)
                #tempCalcDiffPt2.append(calcDiffPt2)
    #               sumDiffPredPt2= sumDiffPredPt2**0.5
                tempCumDiffPt2.append(sumDiffPredPt2)
                
                    
    #-#-# do same for first point lagging
                diffPriorPredPt1 = abs((featr1ValueList[k]) - priorPred1Pt)      # predicted value - actual value
                diffPredPt1      = abs((featr1ValueList[k]) - predF1Point)     # predicted value - actual value
                if(diffPriorPredPt1 < diffPredPt1):
                    calcDiffPt1= diffPriorPredPt1
                    #calcDiffPt1= diffPriorPredPt1**2
                    #calcDiffPt1= diffPriorPredPt1/(4-3*abs(priorPred1Pt))
                    sumDiffPredPt1 += calcDiffPt1
                    tempPredF1Point.append(priorPred1Pt)
                    tempDiffPredPt1.append(diffPriorPredPt1)
                else:
                    calcDiffPt1 = diffPredPt1
                    #calcDiffPt1 = diffPredPt1**2
    #dec2019                   calcDiffPt1 = diffPredPt1 /(4-3*abs(predF1Point))
                    sumDiffPredPt1 += calcDiffPt1
                    tempPredF1Point.append(predF1Point)
                    tempDiffPredPt1.append(diffPredPt1)
                tempCalcDiffPt1.append(0)
    #               tempCalcDiffPt1.append(calcDiffPt1)
    #               sumDiffPredPt1= sumDiffPredPt1**0.5
                tempCumDiffPt1.append(sumDiffPredPt1)
            
                #if k==0: print 'F2222 featr2, (actual) featr1, (predicted) predF1Point = ', "%0.2f" % featr2ValueList[k], '(actual)', "%0.2f" % featr1ValueList[k], '(predicted)',"%0.2f" % predF1Point
        
                #values are only between -1 and 1.0 so if value near 0 the wave is changing faster i.e. larger diff
                # total max diff is 2  but only if at peak so if Y is in middle a lessor worse case size but leave as +2.
                            
            #end For k
            avg1Dist= sumDiffPredPt1/(numPts-1)
            avg2Dist= sumDiffPredPt2/(numPts-1)
            
            if avg2Dist > avg1Dist: meanDiff= avg1Dist
            else: meanDiff= avg2Dist            
    
            #print '\nendOneloop --phz,    totDiff,totPossibleDiff, newCorr prevCorr= ', phaseShiftPrint[cnt],"%0.2f" % totDiff,"%0.2f" % totPossibleDiff," // ", "%0.2f" % (1-(totDiff/ totPossibleDiff)), "%0.2f" % nPSC_Correlation
    
            if False:
            #if totPossibleDiff < 0.01: 
                print( ' error featr1[0] featr2[0] leaFearure totPossibleDiff <0.01 '+str( "%0.2f" %f1or2)+str("%0.2f" % featr1ValueList[0])+str("%0.2f" % featr2ValueList[0])+str( "%0.2f" % totPossibleDiff) )
                leadFeature       = 0      # leadFeature cannot be determined    i
                saveCnt           = 0
                lagPhaseSave = phaseShiftPrint[saveCnt]                      
                    
            else:    #totPossibleDiff > 0 and totPossibleDiff >= totDiff: 
                #v15 newCorr=  1-(2*totDiff/ totPossibleDiff)
                #newCorr=  1-(totDiff/ totPossibleDiff)
                newCorr=     1-meanDiff
                
                if newCorr > nPSC_Correlation:  # If higher than previous loop cyclic corr --save it
                    nPSC_Correlation       = newCorr
                    if avg2Dist < avg1Dist:    #the leader will have the worse higher avg
                    #if f1or2 > 0.0:
                        saveleadFeature      = 1
                        keepPredPoint =tempPredF2Point
                        keepDiffPredPt=tempDiffPredPt2
                        keepCalcDiffPt=tempCalcDiffPt2
                        keepCumDiffPt =tempCumDiffPt2
                        #print("1 keepPredPoint  ="+str(keepPredPoint)+" keepDiffPredPt="+str(keepDiffPredPt))
                    else:
                        saveleadFeature      = 2
                        keepPredPoint = tempPredF1Point
                        keepDiffPredPt= tempDiffPredPt1
                        keepCalcDiffPt= tempCalcDiffPt1
                        keepCumDiffPt = tempCumDiffPt1
                        #print("2 keepPredPoint  ="+str(keepPredPoint)+" keepDiffPredPt="+str(keepDiffPredPt))
                    saveCnt              = cnt
                    lagPhaseSave       = phaseShiftPrint[saveCnt]                     
                    #print( "\n ++SWAPPED corr    cnt,  nPSC_Correlation lagPhaseSave  "+ str(saveCnt)+" "+str("%0.2f" %nPSC_Correlation)+" "+str( saveleadFeature)+" "+str(lagPhaseSave))
        
             #if phz == pie: print '>>>    180 degree Intermittent -- leadFeature, nPSC_Correlation, lagPhaseSave, saveCnt ', leadFeature, "%0.2f" %     nPSC_Correlation, lagPhaseSave, saveCnt
     
      
         #end  For phz
         #end for differing lead feature 1 or 2
                     
         # if phase >= 180 degee or pi radians so is inverse correlation
        if lagPhaseSave == '180' : nPSC_Correlation = -nPSC_Correlation      # phase is over Pi so inverse
    
         #print 'FINAL  nPSC_Correlation, lagPhaseSave,  ',  "%0.2f" % nPSC_Correlation,  saveleadFeature, lagPhaseSave
    
    except Exception as e:
#  #       sys.stdout("error in PSC_Correlation function error = "+ repr(e))        #v10 keep ,
#        if cnt == 0:
        print("\nline 1106 error in PSC_Correlation math.cos  phz featr1 ValueList[k] featr2 ValueList[k]=  "+str(phz)+" f1val "+str(featr1ValueList[k])+" f2val "+str(featr2ValueList[k]))
        print("continue error  sumDiffPredPt1 sumDiffPredPt2  numPts= "+str(sumDiffPredPt1)+" "+str(sumDiffPredPt2)+" "+str(numPts)+"\n")
 
            #print("line 1107error in PSC_Correlation function error = ")  #+ str(e))        #v10 keep ,
        cnt +=1
        status = 'bad_PSC_2_error'
#        nPSC_Correlation=  0
#         keepPredPoint = []
#         keepDiffPredPt= []
#         keepCalcDiffPt= []
#         keepCumDiffPt = []
#         lagPhaseSave  = 0  
#         saveleadFeature= 0                 
       
        return keepPredPoint, keepDiffPredPt,keepCalcDiffPt,keepCumDiffPt, 0, saveleadFeature, lagPhaseSave, status
    
                
    return keepPredPoint, keepDiffPredPt,keepCalcDiffPt,keepCumDiffPt, nPSC_Correlation, saveleadFeature, lagPhaseSave, status
    #======================================================================
def Write_Files(cls, sampClasses, fout, foutInfo, corrList, mssgList, fileText ):
# writes info to both correlation output file and file with KW values and messages about bad values
        
#v42 globals
    global colDelimiter
    global featureHdrRow
    global classCol
    global SampleIdCol
    global minSampleReqdPercent
    global pearson
    global sampleDataList
    global sampleIDList
    global featrNameList
    global allDataMatrix
    global allSampleDict    #v42 allow up to n classes in one input file
    global keepZeroPairs
    global minAbsCorrVal    #v10 added min either pearson or Cyclic corr to add to output file
    
    #print('Add row (corrList) to file len corrlist='+str( len(corrList)) )     #v13 added filetext to name v13

    nSamp     = len(sampleIDList)
    nFeatr      = len(featrNameList[0])
    #fout     = open( outFile, 'w') ##v34 v0
#dec2019    fout.write( '0 row number for ' + str(sampClasses[cls][0])+'\t' +str(sampClasses[cls][0])+'\tLead Feature\tLag Feature\tNumber sample pairs\tCyclic Correlation'\


    fout.write("0 PSC row number\tCohort\tLead Feature\tLag Feature\tNumber sample pairs\tPhase Shift Correlation\tLag Phase (degrees)\tPearson Correlation\tCorrelation Probability")
#    fout.write("\n0 PSC row number\tClass\tLead Feature\tLag Feature\tNumber sample pairs\tPhase Shift Correlation\tLag Phase (degrees)\tLinear Correlation\tFeature 1 Mean \tFeature 2 Mean\tCorrelation Probability\tCorrFlag >abs("+str(minAbsCorrVal))
#WTF    fout.write("\t \t \t \t \t \t PSC \t Lag Phase (degrees) \t Linear Correlation\t Feature 1 Mean \t Feature 2 Mean \tCorrelation Probability\tCorrFlag >abs("+str(minAbsCorrVal))
    fout.write("\t\tRun Info="+fileText+", "+ str(sampClasses[cls][0]))
    fout.write("\n")               #v45 v41 #v40 added file text to header / v18 ##v26 added sample std dev

#v11 putback removed 2nd line                 +'\tLag Phase (radians)\tLinear Correlation \tFeature 1 Mean \tFeature 2 Mean\tCorrelation Probability\tRun Info='+fileText+', '+ str(sampClasses[cls][0])\

    ###v19 we require 3 or more valid samples non-Zero AND must be higher that minSampleReqdPercent pair samples  30% or user selected
    floor = (nSamp * minSampleReqdPercent)-1  ##v19 convert minSampleReqdPercent to a 2 = 0,1,2 pairs
    if floor < 2.0:
        floor = 2.0
    print( 'line1172 in Phase Shift Correlation params num features /num samples / min reqd sample pairs='+str(nFeatr)+str(nSamp)+str(floor+1) ) ##v31 v19

    print("line1174 corrlist="+str(corrList[0]))
    for i in range(0,len(corrList)):
        #v42 if #number samples < floor do not print row
        #v50if corrList[i][3] >= floor:
        if corrList[i][4] >= floor:
            for j in range(0,len(corrList[i])):
                fout.write( str(corrList[i][j])+'\t')  ##18
            fout.write( '\n')  ##v40 remove file text  //
    #fout.close()


    foutInfo.write( 'Messages for bad data values'+'\t'+'Run Info='+fileText+ '\n') ##v40 added file text to header /v18

#     if cls == 0: 
#         for i in range(0,len(mssgList)):
#             for j in range(0,len(mssgList[i])):
#                 foutInfo.write( str(mssgList[i][j])+'\t')     ##20      print feature name to file
#             foutInfo.write( '\n')  
#         foutInfo.write( '\n') 

#ucmucosa remove print
    foutInfo.write( 'KW info for Features\t'+'Measurable samples='+str(featrNameList[1][0])+' \tRun Info='+fileText+ '\n') ##v40 added file text to header /v18
 
#     for i in range(0,len(featrNameList[1])):
#         foutInfo.write( str(featrNameList[0][i])+'\t')      ##20       print feature name to file
#         foutInfo.write( str(featrNameList[1][i])+'\t')      
#         foutInfo.write( '\n')  

    del corrList
    del mssgList 


    return      fout, foutInfo

#======================================================================
def Start_Correlation( CSVfile, outFile, outFileInfo, featureStartColNum=3, featureEndColNum=40, fileText='sampleN'):
                          
#v42                          ,featureHdrRow=1,SampleIdCol=1,minSampleReqdPercent=0.15, 
#v42                          pearson = 'no',keepZeroPairs= 'yes',colDelimiter='\t'):

    # NOTE: if Item value is zero or text (n/a nan other) it will NOT be included in rank
    #         those Items will be given rank =0 and not used in further calculations

    # INPUT
    # 1. CSVfile = comma delimited file input with individual samples as rows
    #       and each feature -- metabolite, VOC, bacteria, fungi, other as columns
    #     column                    c1              c2          c3       c4         c5
    #  Possible heading row= sample ID      metadata    Sucrose     E.Coli       fungi N
    #                          Person 1      Male        2.35        50607     985
    #      NOTE: ss can have mulitple sample types in it - disease 1,2,3, normal)
    #  2. featureHdrRow=1
    #  3. featureStartColNum ='2' the first data column to be used in the correlation
    #  4 featureEndColNum ='4' the last data column to be used in the correlation
    #            indicates to not use the value in calculations or substitute the minimum
    #            value of the other values for the item and include it in calculations
    #            default is do not use No = 'no'       else is yes.
    #     5. SampleIdCol=1
    #     6. SampleDataRowStart=2,
    #     7. SampleDataRowEnd=10
    #  8.     minSampleReqdPercent = there must at least 3 values or user selected amount to be included in corr calc. ##v19
    #  9. colDelimiter ='\t' applies to how fields are delimited in ss -default is comma delimted
##V31
    #  10. pearson = 'yes'      NEEDED for CyCLIC CORRELATION FLAG or else do Spearman Rank correlation.
##v31      #       11. keepZeroValues = EXACTLY 'yes'     For data values that are zero or not measureable will be included 
    #  11. keepZeroPairs = EXACTLY 'N'       If both correlation pair data values = zero or not measureable then NOT included
  
##v31      if (keepZeroValues != 'yes'): print '--------- do NOT use ZERO or BAD VALUES in Rank'
##v34a v0  add outFile & outFileInfo to call from Galaxy
##
##v42  CHANGE EXPECT PARAMS THIS WAY:::         Tab delimited with NO extraneous rows or columns beyond the data values !!!
#     row 1 is feature names column 1 dont car column 2 is dont care       col 3 and contiguously to last feature name in column N 
#     row 2 to end of samples in row N
#     column 1 is sample name
#     column 2 is class/cohort name -- all classes MUST be in contiguous rows of the tab delimited text file 
#     Column 3 or greater and contiguously to last feature column N    are values or N/A or text (both are excluded values)
#end V42

#v42 globals
    global colDelimiter
    global featureHdrRow
    global classCol
    global SampleIdCol
    global minSampleReqdPercent
    global pearson
    global sampleDataList
    global sampleIDList
    global featrNameList
    global allDataMatrix
    global allSampleDict    #v42 allow up to n classes in one input file
    global keepZeroPairs
    global normwAllVals4Feat


    print( 'PARAMETERS used')
    if (keepZeroPairs == 'N'): print( '-- PSC does NOT use feature 1 &2 correlation pair values if both =ZERO or BAD VALUES')
    else:                         print( '--YES use ZERO value correlation pairs or Immeasurable VALUES in Rank')
    if (pearson != 'yes'): print( '>> using Spearman RANK correlation')
    else:                   print('>> using Pearson correlation')
    print( '-- Using minmum sample percent= '+ str(minSampleReqdPercent) +' to determine feature correlation \n \n' )     ##v19

    fout    = open( outFile, 'w') ##v34 v0
    foutInfo    = open( outFileInfo, 'w') ##v34 v0

    fp = open ( CSVfile, 'rU' )        #v42 universal read
    CVSdata = fp.read()
    fp.close()
    allDataMatrix= []
    featrNameList= []
    corrList= []
    mssgList= [] ###v14

    
# python counts from ZERO  keep input param value as intuitive cnt from 1!!!!!!!!!!
#     featureStartColNum= 0 
#     SampleDataRowStart= 1
#     SampleDataRowEnd = 9999;      #v42 doesn't matter #v13
#     SampleIdCol= 0

#      print 'params='+CSVfile + fileText+featureHdrRow+ featureStartColNum+ featureEndColNum+ \
#                            SampleIdCol+     colDelimiter+ keepZeroValues+'\n'
    
#v45    if ( featureStartColNum <= featureEndColNum):     endcol is deduced with first row read       
    # split the input CVS file and verify dimensions
    fileError = Parse_CVSfile2Dict( CVSdata,featureStartColNum, featureEndColNum)  #v45 endcolNum is ignored

    #print ' dict of sample key(row# in SS-2) -> feature values ' , allSampleDict         

    if fileError != 'true' :
        mssgList=[]
#        if not normwAllVals4Feat:
        mssgList= clean_Dict_Data_Values() #v42 change name & calling seq // v32 removed #v31 added pearson flag #v34 add minSampleReqdPercent
        sampClasses,foutInfo = KW(foutInfo )    #v42 determine KW for each feature based on the n classes of samples

        for cls in range( len(sampClasses)):                
            corrList,mssgList= PSC_Calc_and_StndErr(cls, sampClasses, mssgList) # v31 v32 add param##v19 add param
            fout, foutInfo= Write_Files( cls, sampClasses, fout, foutInfo,corrList, mssgList, fileText )
            if corrList == []:      
                print('<<<< NO correlation data to write to output FILE for class= '+str(sampClasses[cls][0]) )

    else: ##v31
        print( 'Parse_CVSfile file read error')
#     else:
#         print'ERROR featureStartColNum > featureEndColNum ', featureStartColNum, '>',featureEndColNum

    fout.close()
    foutInfo.close()
        
        
#v334 v0 galaxy          return sampleDataList, corrList, allDataMatrix, featrNameList,sampleIDList,mssgList

    return

###================ Galaxyy mods below      v34a v0 ============
def stop_err( msg ):
    sys.stderr.write( msg )
    sys.exit()


###================###================###================###================###================
def checkPrivilege(user):
    tool_privilege      = 1
    #user              = sys.argv[-1]
    print( user)
    return GalaxyTools.getPrivilege(user,tool_privilege)

###================###================###================###================###================

def main():
    
#v42 globals
    global colDelimiter
    global featureHdrRow
    global classCol
    global SampleIdCol
    global minSampleReqdPercent
    global pearson
    #global sampleDataList
    global sampleIDList
    global featrNameList
    global allDataMatrix
    global allSampleDict    #v42 allow up to n classes in one input file
    global keepZeroPairs
    global KWprobCutoff
    global minAbsCorrVal    #v10 added min either pearson or Cyclic corr to add to output file
    global maxOutliers      #uc mucosa change be able to set (currently hardcode) max outlier     only normalized scores > 0.1 or only < 0.1 pairs to output for Chart AND psc output
    global featSample4ChartFH     #uc mucosa chfile of feature1 feature2 pairs that meet min sample no outliers    psc corr>0.8 peasron < 0.8 to use to create charts
    global normwAllVals4Feat   #2019dec06  see if 2nd input is in Sine value range then set global flag
    # check the command line
    sys.stdout.write(' starting Phase Shift Correlation main module. Arguments=')
    sys.stdout.write(str(sys.argv[1:]))

#v50 turned off params
#     try:
    csvFile     = str(sys.argv[1])
#2019dec06  if input is Sine values NOT raw data then set global flag saying so
    normwAllVals4Feat = False
    if( len(sys.argv) > 2 and str(sys.argv[2].lower()) == "sinevalues"):  normwAllVals4Feat = True

#         minAbsCorrVal          = abs(sys.argv[2])     
    runID     = "Rifaximin_RDP_Metabolome Jan2020 analysis"     
    featSample4ChartFN    = str(sys.argv[1][:-4])+"-PSC-Chart_Data.txt"   
    
    maxOutliers = 1
#uc mucosa     runID      = str(sys.argv[2])   
#uc mucosa      outFile  = str(sys.argv[3])      
#         outFileInfo            = sys.argv[4]
#         featureStartColNum       = int(sys.argv[5])
# #V45          featureEndColNum      = int(sys.argv[6]) set to zero below
#         fileText                 = sys.argv[7]      # save settings for this run in the output file
#         pearson                    = sys.argv[8]      #default No
#         KWprobCutoff             = sys.argv[9]       #v43     on or off //default p= 0.05
#         keepZeroPairs              = sys.argv[10]     #default = 'Y'         
#     except:
#        stop_err( 'Usage: python corrNet vN.py input arguments failed' )
# 
#     featureEndColNum  = 0    #v45     !!!! not used anymore 
# 
#     print       "sys arguments=", sys.argv[1:8]
#     if pearson == 'pearson':
#         pearson = 'yes'
#     else:
## no good for cyc corr!!         
    pearson = 'no'

    colDelimiter = '\t'

#v50 bb test input        
#ucmucosa set min high
    minAbsCorrVal = 0.7         #dec2019 from Galaxy xml default will be 0.0
#11a test linear neg correlation winners see why
#     csvFile = '/Users/daddio/Desktop/Cyclic-Corr-NASHsamples/CONTROLonly-col5-linearWinningFeatures-July2016-v8.txt'

    featureStartColNum = 3       #second column is LIHC iCluster
    #outFile            = 'LIHC_Testing_Folder/Output/LIHC_PSC_Corr_Output.txt'
    outFile     = str(sys.argv[1][:-4])+"-PSC-F1_F2_List.txt"       
    outFileInfo = str(sys.argv[1][:-4])+"-PSC_KW_info.txt"
    print_corr_value   = 0 # radian offset to print to limit the output 

#v11a TODO      featureStartColNum = 43      # -1 later in python    3&6       or 4-54       1544
    featureEndColNum   = 0      # not used v50???
#v10b     fileText            = 'Cyclic-Correlation-printsubset_v10 w NASHonly samples and ALL Metabolites-col43-july2016-v8.txt'
    fileText           =  'Rifaximin_RDP_Metabolome 3 cohorts'

#!!!!!person MUST be YES for this to work
    pearson            = 'yes'     #!!!!!     v12 LEAVE THIS AS YES ELSE THE DATA TO CYCLIC CORRELATION GETS CHANGED TO RANKS!!!!
    KWprobCutoff =      0.05

    featSample4ChartFH= open(str(sys.argv[1][:-4])+"-PSC_Chart_Data.txt", "w")

    featSample4ChartFH.write("0\t0\t"+fileText+"\n")
    featSample4ChartFH.write("\n0\t0\tActual Lead Feature \ttActual Lag Feature \tPhase Lead Feature \tPhase Lag Feature \tPredicted Lag Feature\tCohort\t Phase Shift Correlation \t Lag Phase Shift \t Pearson Rho")
    featSample4ChartFH.write("\t\tStarting Phase Shift Correlation main module. Arguments=")
    featSample4ChartFH.write(str(sys.argv[1:])+"\n")

# 
# 
#v42 REQUIRED ROW AND COLUMN LOCATIONS FOR THE FOLLOWING DATA  !!!!
    featureHdrRow     = 1
    classCol        = 2         #really python 1    #todo changed for tumor cancer rna data     col 3 (py 2) actual cancer type
    SampleIdCol           = 1        #really 0
    minSampleReqdPercent= 0.25      #uc mucosa made higher set to TODO changed for cancer RNA to 5% from 30% of both sample pairs must be non-zero  # REMOVE FROM XML
    keepZeroPairs         = 'N'      #v10 circ corr change from 'Y' 

    colDelimiter         = '\t'
        
    
    Start_Correlation( csvFile, outFile, outFileInfo, featureStartColNum, featureEndColNum,
                          fileText)
    print( " >>>     Phase-Shift-Correlation-PSC-Master Fini")
    featSample4ChartFH.close()
    return
##
##
#if __name__ == '__main__':

#        from minings.utility.utility import GalaxyTools
#        if    checkPrivilege(sys.argv[-1]):
#     sys.exit(main()) 
#        else:
#         print 'Need to request access to this tool '

if __name__ == "__main__": main()